#!bin/bash

###### Compile pdf cheatsheet form txt file
# This script converts the textfile to tex, then compiles it using latexmk.
# Cheatsheet will be a mdboxed latex file, where each box is a differently
# colored section with a title, and content holding a table of 2 columns.

###### Algorithem
# - Init empty tex file and write tex preamble.
# - Start interating through each line of txt file.
# - For each section label (line starting with '#'), start a mdframed
#   environment with that line as the title.
# - Keep adding following lines to mdframe until empty line is encountered,
#   then close it.
# - When eof reached, close environments.
#
# - Compile tex file using latexmk.

###### Notes
# - The preamble tex file must define a mdframed environment named "frame"

###### Setup script parameters
# Input txt file
ftxt="vim_keybindings.txt"
# Output file name (no extension)
fout="cheatsheet_nvim_RTbecard"
# file holding latex preamble
fpreamb="tex_preamble.tex"

# init output file
echo "Writing preamble..."
cat "$fpreamb" > "${fout}.tex"
echo "" >> "${fout}.tex"

# Start latex document
echo '\\begin{document}' >> "${fout}.tex"
echo "" >> "${fout}.tex"

echo "Reading txt file..."
###### Itrerate though file
while read -r line; do
    echo "$line"

    if $(echo "$line" | grep -qP '^#.+$'); then
        #### Section title
        echo "---- Section title ----"
        title=$(echo "$line" | grep -oP '(?<=#\s).*$')
        echo "  \\\sloppy" >> "${fout}.tex"
        echo "  \\\begin{framed}{${title}}" >> "${fout}.tex"
        echo '  \\begin{tabularx}{\\textwidth}{A B}' >> "${fout}.tex"

    elif $(echo "$line" | grep -qP '^\s*$'); then
        #### Section end (empty line)
        echo "---- Section end ----"
        echo '  \\end{tabularx}' >> "${fout}.tex"
        echo '  \\end{framed}' >> "${fout}.tex"
        echo '' >> "${fout}.tex"

    else
        #### Section Content
        echo "---- Section content ----"
        echo "      $line \\\\\\" >> "${fout}.tex"
    fi

done < "$ftxt"

echo '\\end{document}' >> "${fout}.tex"


###### Compile latex doc
latexmk -f -pdf -interaction=nonstopmode "${fout}.tex"
latexmk -c
