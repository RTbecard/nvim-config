#!/bin/bash

# The relative paths in this script expect that the repository was cloned into
# your home directory:
# "cd ~/; git clone https://gitlab.com/RTbecard/nvim-config.git"  

# Link init.vim
ln -srf ./init.vim ../.config/nvim/init.vim

# Init and link ftplugin 
ln -srf ./ftplugin ../.config/nvim/ftplugin

