set encoding=utf-8
" --- Key mappings
imap jk <Esc>
" Paste copied text (instead of detelcted text)
nnoremap <leader>p "0p
let mapleader=";"

" Margin
set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey
set textwidth=80
set formatoptions=c
" :help fo-table

" formatting helper
set list
" set listchars=eol:↓,tab:\ \ ┊,trail:●,extends:…,precedes:…,space:·
set listchars=tab:→\ \ ,trail:·,extends:…,precedes:…

" enable syntax highlighting
syntax enable
" show line numbers
set number
" set tabs to have 4 spaces
set ts=4
set softtabstop=4
" indent when moving to the next line while writing code
set autoindent
" expand tabs into spaces
set expandtab
" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4
" show a visual line under the cursor's current line
set cursorline
" show the matching part of the pair for [] {} and ()
set showmatch
" enable all Python syntax highlighting features
let python_highlight_all = 1
" Enable mouse scrolling
set mouse=a
" Limit size of spell check window (don't take up full screen)
set spellsuggest=fast,10
" Preview window
set previewheight=30

" Use own julia package
let g:polyglot_disabled = ['julia-vim.plugin']
let g:polyglot_disabled = ['latex']

set nocompatible
" Vimplug - Load these plugins
call plug#begin(stdpath('data') . '/plugged')
Plug 'ycm-core/YouCompleteMe'
Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdtree'
"Plug 'vim-syntastic/syntastic'
Plug 'dense-analysis/ale'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-surround'
Plug 'lervag/vimtex'
Plug 'Yggdroot/indentLine'
Plug 'tpope/vim-commentary'
" Broad language support (does not work with julia)
Plug 'sheerun/vim-polyglot'
"Themes
Plug 'lifepillar/vim-solarized8'
Plug 'morhetz/gruvbox'
Plug 'phanviet/vim-monokai-pro'

Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && sh install.sh'  }
" origional version with yarn did not work. Use sh file instead
"Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
"Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
" Vim terminal
Plug 'jpalardy/vim-slime'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'moll/vim-bbye'
" R support
Plug 'jalvesaq/Nvim-R'
" Ctags viewing
Plug 'preservim/tagbar'
" Fuzzy file search
Plug 'ctrlpvim/ctrlp.vim'

call plug#end()

" Radian support for Nvim-R
let R_app = "radian"
let R_cmd = "R"
let R_hl_term = 0
let R_args = []  " if you had set any
let R_bracketed_paste = 1
let R_assign=0  " disable automatic mapping of _ to <-

" Disable indent plugin for certain filetypes
let g:indentLine_fileTypeExclude = ['tex', 'markdown']

" Linting options
let g:ale_echo_msg_format = '%linter% says %s'
let g:ale_python_pylint_options = '--disable=missing-docstring,invalid-name --extension-pkg-whitelist=PyQt5'

" vim-colors-solarized
set termguicolors
set t_Co=256
"syntax enable
"set background=dark
colorscheme monokai_pro

set splitbelow

" View tagbar
nmap <F8> :TagbarToggle<CR>

" lightline
let g:lightline = 
      \ {'colorscheme': 'wombat',
      \'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'bufnum', 'relativepath', 'modified'  ] ]
      \ },
      \'inactive': {
      \   'left': [[ 'readonly', 'bufnum', 'relativepath', 'modified' ] ] 
      \ }
      \ }
" gitgutter
" Show modified lines in left margin
function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction
set statusline+=%{GitStatus()}

" always show status line with file path
set laststatus=2
set statusline+=%F

" youcompleteme
let g:ycm_python_binary_path = '/usr/bin/python3'
let g:ycm_autoclose_preview_window_after_insertion = 1
nmap <S-H> :YcmCompleter GetDoc<CR><C-W>j
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_enable_semantic_highlighting = 1
let g:ycm_enable_inlay_hints = 1
let g:ycm_clear_inlay_hints_in_insert_mode = 1

" Vimtex
let g:tex_flavor = 'latex'
let g:vimtex_complete_ignore_case = 1 " Case insensitive reference searches
let g:vimtex_syntax_conceal = {
      \ 'accents': 1,
      \ 'cites': 2,
      \ 'fancy': 0,
      \ 'greek': 1,
      \ 'math_bounds': 1,
      \ 'math_delimiters': 1,
      \ 'math_fracs': 0,
      \ 'math_super_sub': 0,
      \ 'math_symbols': 1,
      \ 'sections': 1,
      \ 'styles': 1,
      \}


" Utilsnips
" Remap tab to make compatable with youcompleteme
" let g:UltiSnipsExpandTrigger="<c-j>"
" let g:UltiSnipsJumpForwardTrigger="<c-b>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" let g:UltiSnipsListSnippets="<leader>l"

" Julia
" COnvert latex to unicode as you type
let g:latex_to_unicode_auto = 1

" Matchit
" " Enable plugin
runtime macros/matchit.vim

" slime configuration
let g:slime_target = 'neovim'
" Send current word
nmap <Leader>w bve:SlimeSend<cr>
nmap <Leader>j <Plug>SlimeMotionSend
"nnoremap <Leader>w bve:SlimeRegionSend<cr>
" Send current Line
inoremap <C-C><C-C> <Esc>:SlimeSend <cr>i
" Send current selection
vnoremap <C-C><C-C> '<,'>SlimeRegionSend
" Send current cell
nmap <leader>s <Plug>SlimeSendCell

" ------ Terminal kemaps
"  Exit term mode, go to end of buff, jump to previous window
tnoremap jl <C-\><C-N>G<C-W><C-P>
" Exit term mode, stay in buffer
tnoremap jk <C-\><C-N>G

"let g:vimtex_syntax_conceal_disable = 1
set concealcursor=""

" =============================================================================
" ------ Custom commands ------
" Edit cheatsheet
command CheatsheetEdit tabe ~/nvim-config/cheatsheet/vim_keybindings.txt
" Compile and open cheatsheet
command CheatsheetCompile ! cd ~/nvim-config/cheatsheet &&
            \ sh ./compile_sheet.sh &&
            \ xdg-open ./cheatsheet_nvim_RTbecard.pdf
" Edit init.vim
command InitEdit tabe ~/nvim-config/init.vim

" =============================================================================
" Open terminal console at bottom of screen
function! FunOpenConsoleBelow(shell)
    " use execute to evaluate argument before passing to command
    execute "split term://"a:shell
    " wincmd J
    res 20
    set winfixheight
    normal G
endfunction
command! -nargs=1 OpenConsoleBelow call FunOpenConsoleBelow(<f-args>)

" Open terminal console at bottom of screen
function! FunOpenConsole(shell)
    " use execute to evaluate argument before passing to command
    execute "vsplit term://"a:shell
endfunction

command! -nargs=1 OpenConsole call FunOpenConsole(<f-args>)

" Run script in console
function! FunRunScript()
  " Get script launching command
  echom "Running current file in shell"
  if &filetype == "r"
    let s:cmd_pre = "Rscript "
  elseif &filetype == "julia"
    let s:cmd_pre = "julia "
  elseif &filetype == "python"
    let s:cmd_pre = "python3 "
  else
    echom "Invalid file type.  Must be R, Python, or Julia."\
    return 1
  end

  " Get current file path
  let s:file = expand('%')
  let s:cmd_post = s:cmd_pre . s:file
  echom s:cmd_post

  " Open a new scratch buffer in new tab
  tabnew
  setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile nowrap
  execute 'read !'. s:cmd_post
  setlocal nomodifiable
endfunction
command! RunScript call FunRunScript()

nmap <F5> :RunScript<CR>

" Wordcount for latex
command! TexCount execute "!texcount " . expand("%p") . " -sum"


" =============================================================================
" cpp docs
function! s:JbzCppMan()
    let old_isk = &iskeyword
    setl iskeyword+=:
    let str = expand("<cword>")
    let &l:iskeyword = old_isk
    execute 'Man ' . str
endfunction
command! JbzCppMan :call s:JbzCppMan()
" Rebind K for cpp files
au FileType cpp nnoremap <buffer>K :JbzCppMan<CR>
