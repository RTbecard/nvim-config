" Ignore
" function/variable name style checks
" Too many varaible warnings
let g:syntastic_quiet_messages = {
            \ 'regex': ".* name .* doesn't conform to snake_case naming style.*" .
            \"\\|.*Too many local variables.*".
            \"\\|.*Too many instance attributes.*".
            \"\\|.*Too many statements.*".
            \"\\|.*More than one statement.*"}

" Enable code folding
set foldmethod=indent

autocmd BufWritePre * %s/\s\+$//e

let b:slime_cell_delimiter="###"
