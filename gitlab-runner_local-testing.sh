#!/bin/bash

# This is an offline test of the gitlab CI system.  This should be used before 
# pusing updates that affet builds.

sudo gitlab-runner exec docker --docker-pull-policy=if-not-present compile-cheatsheet

