#!/bin/bash

# This script is for building docker images on your local machine.
# Built images can be uploaded to dockerhub for use in gitlab CI
# Test locally with gitlab-runner

# Start docker daemon
sudo service docker start

# Build Docker image
sudo docker image build \
	-t rtbecard/latex-full \
	--file ./dockerfile-latex-full-bionic \
	./

# Show build images
sudo docker image list

# Loginto dockerhub with "sudo docker login"
# sudo docker push rtbecard/latex-full
