### nvim-config

This is my Neovim setup for new machines (tested on Ubuntu 20.10).  The goal was to reproduce and syncronize my full setup as fast as possible across multiple machines.  Simply:

1. Clone this repository to your home directory (`~/`).
2. Install Neovim `sudo apt install neovim`.  Add the neovim ppa to get the latest stable release `sudo add-apt-repository ppa:neovim-ppa/unstable; sudo apt-get update; sudo apt-get install neovim`.
3. Run `sh Install_vim-plug.sh` to install the plugin manager, `vim-plug`.
4. Then run the repository script `Init_symlinks.sh`.  The will make symlinks in your home directory structure which will allow Neovim to access the config files in this repository.
You may have to create the neovim config directories beforehand with `mkdir ../.config/nvim`.
5. Open vim then install the plugins with `:PlugInstall`, then close vim.  This can also be done directly from the shell with `vim +'PlugInstall --sync' +qa`
6. Run `Compile_YouCompleteMe.sh` to compile code-completion engine.  If this fails, try installing `sudo apt install python3-dev cmake`.

The setup should now be complete!

Note that the symlink script uses relative paths, so this repository must be located in your home directory for them to work: `cd ~/; git clone https://gitlab.com/RTbecard/nvim-config.git`.
Some `init.vim` parameters I set also require the name of this repository folder to be preserved (i.e. do not rename this folder after cloning).

This setup was geared towards writing _python_ and _latex_.

#### Cheat sheet

`cheatsheet/compile_cheatsheet.sh` will make a latex pdf of relevant key bindings for this config from the template file `cheatsheet/vim_keybindings.txt`.
The pdf is compiled with `latexmk` and this script was tested on machines which had the package `texlive-full` installed.
You can view the latest compiled version of the cheat sheet <a href="https://gitlab.com/RTbecard/nvim-config/-/jobs/artifacts/master/raw/cheatsheet/cheatsheet_nvim_RTbecard.pdf?job=compile-cheatsheet" target="_new">here</a>.

While in Neovim, you can use the command `:CheatsheetEdit` to edit the contents of the sheet in Neovim and then `:CheatsheetCompile` to compile and open the latest version of the cheat sheet in your default pdf viewer.

The template for the `txt` file is simple where keybindings are divided into titled sections:
- New sections are denoted by lines starting with `#`, where the following characters are the section title.
- Section contents are treated as latex table rows with two columns.  The column separator is `&` and the newline character indicating the end of the row is automatically be added by the script.
- An single empty line denotes the end of a section.

While editing the cheat sheet template, this formatting should be very obvious.
Make sure to *leave an empty newline at the end of the template* to indicate the closing of the last section.
Also, *be mindful of special characters which require escaping in latex tables*, such as curly braces `\{` and `\}`.
